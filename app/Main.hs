module Main where
import Control.Monad
import GHC.AssertNF
import Data.IORef

main :: IO ()
main = do
  ref <- newIORef (0 :: Int)
  void $ atomicModifyIORef' ref (\x -> (x + 1, ()))
  assertNF ref

